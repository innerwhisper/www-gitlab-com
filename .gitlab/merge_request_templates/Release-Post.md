Release Post

### General Contributions

General contributions from the team.

Due date: MM/DD (6th working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release
- [ ] Intro
- [ ] MVP
- [ ] Webcast link
- [ ] Upgrade barometer
- [ ] Main features
- [ ] Secondary features
- [ ] Other features
- [ ] Performance improvements
- [ ] Omnibus improvements
- [ ] Deprecations
- [ ] Extras
- [ ] Documentation links
- [ ] Authorship (author's data)

### Content Review

Due date: MM/DD (4th working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release ~review-in-progress
- [ ] General review (PM)
  - [ ] Check Features' names
  - [ ] Check Features' availability (CE, EES, EEP)
  - [ ] Check Documentation links
  - [ ] Add `data/promo.yml`
  - [ ] Check all images' size - compress whenever > 300KB
  - [ ] Meaningful links (SEO)
  - [ ] Check links to product/feature webpages
- [ ] Copyedit (Sean P, Rebecca, or Marcia)
  - [ ] Title
  - [ ] Description
  - [ ] Grammar, spelling, clearness (body)
- [ ] Final review (Job)

### Structure & Styles

Add HTML/CSS structure to style the blog post, review markup. Technical Writing / Frontend / UX.

Due date: MM/DD (1st working day before the 22nd)

- [ ] Label MR: ~"blog post" ~release ~review-structure
- [ ] Check frontmatter (entries, syntax)
- [ ] Check `<!-- more -->` separator
- [ ] Add cover image `image_title` - compressed
- [ ] Add social sharing image `twitter_image` - compressed
- [ ] Apply semantic HTML (sections, divs, classes, ids)
- [ ] Check image shadow `{:.shadow}`
- [ ] Check images' `ALT` text
- [ ] Videos/iframes wrapped in `<figure>` tags (responsiveness)
- [ ] Columns (content balance between the columns)
- [ ] Badges consistency (applied to all headings)
- [ ] Remove any remaining instructions
- [ ] Remove HTML comments
- [ ] Run deadlink checker
- [ ] Update release template with any changes (if necessary)
