---
layout: markdown_page
title: "Technical Writing"
---

[Technical Writing](/handbook/product/technical-writing/) was transferred to the [Product](/handbook/product/) Team.
